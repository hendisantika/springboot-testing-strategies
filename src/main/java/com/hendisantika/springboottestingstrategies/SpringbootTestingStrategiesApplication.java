package com.hendisantika.springboottestingstrategies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootTestingStrategiesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootTestingStrategiesApplication.class, args);
    }

}
