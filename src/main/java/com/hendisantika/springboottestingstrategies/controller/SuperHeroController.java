package com.hendisantika.springboottestingstrategies.controller;

import com.hendisantika.springboottestingstrategies.domain.SuperHero;
import com.hendisantika.springboottestingstrategies.repository.SuperHeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-testing-strategies
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-29
 * Time: 06:03
 */
@RestController
@RequestMapping("/superheroes")
public class SuperHeroController {
    @Autowired
    private SuperHeroRepository superHeroRepository;

    @GetMapping("/{id}")
    public SuperHero getSuperHeroById(@PathVariable int id) {
        return superHeroRepository.getSuperHero(id);
    }

    @GetMapping
    public Optional<SuperHero> getSuperHeroByHeroName(@RequestParam("name") String heroName) {
        return superHeroRepository.getSuperHero(heroName);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addNewSuperHero(@RequestBody SuperHero superHero) {
        superHeroRepository.saveSuperHero(superHero);
    }
}
