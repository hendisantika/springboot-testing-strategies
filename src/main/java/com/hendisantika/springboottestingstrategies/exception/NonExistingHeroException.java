package com.hendisantika.springboottestingstrategies.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-testing-strategies
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-29
 * Time: 06:00
 */
public class NonExistingHeroException extends RuntimeException {
}
